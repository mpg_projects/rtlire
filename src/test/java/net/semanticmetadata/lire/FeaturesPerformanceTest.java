package net.semanticmetadata.lire;

import com.sun.management.OperatingSystemMXBean;
import junit.framework.TestCase;
import net.semanticmetadata.lire.imageanalysis.CEDD;
import net.semanticmetadata.lire.imageanalysis.FCTH;
import net.semanticmetadata.lire.imageanalysis.LireFeature;
import net.semanticmetadata.lire.imageanalysis.SimpleColorHistogram;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Michael on 18.08.2015.
 */
public class FeaturesPerformanceTest extends TestCase{

    private String[] testFiles = new String[]{"img01.jpg", "img02.jpg", "img03.jpg", "img04.jpg", "img05.jpg", "img06.jpg", "img07.jpg", "img08.jpg", "img09.jpg", "img10.jpg"};
    //private String testFilesPath = "src/test/resources/small/";
    private String testFilesPath = "E:\\liretestfiles\\";

    //This are the most common features, can be used for iteration
    //String[] classArray = {"CEDD", "EdgeHistogram", "FCTH", "ColorLayout", "PHOG", "JCD", "Gabor", "JpegCoefficientHistogram", "Tamura", "LuminanceLayout", "OpponentHistogram", "ScalableColor"};


    public void testEmAll(){
        String[] classArray = {"CEDD", "FCTH", "JCD", "PHOG"};

        OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(
                OperatingSystemMXBean.class);

        for (String s: classArray) {
            try {
                testExtraction(s);

                // What % CPU load this current JVM is taking, from 0.0-1.0
                System.out.println(osBean.getProcessCpuLoad());
                // What % load the overall system is at, from 0.0-1.0
                System.out.println(osBean.getSystemCpuLoad());

                testRetrieval(s);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void testExtraction(String feature) throws IOException {
        //CEDD sch = new CEDD();
        LireFeature feat = null; //new FCTH();
        try {
            feat = (LireFeature) Class.forName("net.semanticmetadata.lire.imageanalysis." + feature).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        LireFeature sch = feat;
        sch = feat;
        BufferedImage image = ImageIO.read(new FileInputStream(testFilesPath + testFiles[0]));
        System.out.println("###### " + feature + " ######" );
        System.out.println("image = " + image.getWidth() + " x " + image.getHeight());
        long startTime = System.currentTimeMillis();
        sch.extract(image);
        System.out.println("Extraction time " + feature + " : " + (System.currentTimeMillis()-startTime));
        //System.out.println("sch = " + sch.getStringRepresentation());
    }


    public void testRetrieval(String feature) throws Exception {

        long averageExtractionTime = 0;

        LireFeature[] acc = new LireFeature[testFiles.length];
        LinkedList<String> vds = new LinkedList<String>();
        for (int i = 0; i < acc.length; i++) {
            long startTime = System.currentTimeMillis();
            LireFeature feat = null; //new FCTH();
            feat = (LireFeature) Class.forName("net.semanticmetadata.lire.imageanalysis." + feature).newInstance();
          //  System.out.println("Extracting from number " + i);
            acc[i] = feat;
            acc[i].extract(ImageIO.read(new FileInputStream(testFilesPath + testFiles[i])));
            averageExtractionTime+=(System.currentTimeMillis()-startTime);
           // vds.add(acc[i].getStringRepresentation());
        }
        System.out.println("Average Extraction time " + feature + " : " + averageExtractionTime/10);

       // Not needed so far, maybe later
       // System.out.println("Calculating distance for " + testFiles[5]);
       // for (int i = 0; i < acc.length; i++) {
       //     float distance = acc[i].getDistance(acc[5]);
       //     System.out.println(testFiles[i] + " distance = " + distance);
       // }
    }


}
