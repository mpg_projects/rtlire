package net.semanticmetadata.lire;

import com.sun.management.OperatingSystemMXBean;
import junit.framework.TestCase;
import net.semanticmetadata.lire.imageanalysis.CEDD;
import net.semanticmetadata.lire.imageanalysis.FCTH;
import net.semanticmetadata.lire.imageanalysis.LireFeature;
import net.semanticmetadata.lire.imageanalysis.SimpleColorHistogram;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Michael on 18.08.2015.
 */
public class ObjectDetectTest extends TestCase{

    boolean createHTML = true;

    String[] testFiles = new String[]{"img1.jpg", "img2.jpg", "img3.jpg", "img4.jpg", "img5.jpg", "img6.jpg", "img7.jpg", "img8.jpg", "img9.jpg", "img10.jpg", "img11.jpg", "img12.jpg", "img13.jpg", "img14.jpg", "img15.jpg"};
    //private String testFilesPath = "src/test/resources/small/";
 // String[] testFiles = new String[]{"img1.jpg", "img2.jpg"};


    private String testFilesPath = "E:\\liretestfiles\\";

    //This are the most common features, can be used for iteration
    //String[] classArray = {"CEDD", "EdgeHistogram", "FCTH", "ColorLayout", "PHOG", "JCD", "Gabor", "JpegCoefficientHistogram", "Tamura", "LuminanceLayout", "OpponentHistogram", "ScalableColor"};


    public void testEmAll(){
        //String[] classArray = {"CEDD", "FCTH", "JCD", "PHOG"};
        String[] classArray = {"FCTH"};

        OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(
                OperatingSystemMXBean.class);

       // testPaintImage();
      //  TestPaintImage showImage = new TestPaintImage();

        for (String s: classArray) {
            try {
                //testExtraction(s);

                // What % CPU load this current JVM is taking, from 0.0-1.0
                System.out.println(osBean.getProcessCpuLoad());
                // What % load the overall system is at, from 0.0-1.0
                System.out.println(osBean.getSystemCpuLoad());

                testRetrieval(s);

           // } catch (IOException e) {
          //      e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       // createHTML();
    }


    public void testExtraction(String feature) throws IOException {
        //CEDD sch = new CEDD();
        LireFeature feat = null; //new FCTH();
        try {
            feat = (LireFeature) Class.forName("net.semanticmetadata.lire.imageanalysis." + feature).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        LireFeature sch = feat;
        sch = feat;
        BufferedImage image = ImageIO.read(new FileInputStream(testFilesPath + testFiles[0]));
        image.getSubimage(20,20,20,20);
        System.out.println("###### " + feature + " ######" );
        System.out.println("image = " + image.getWidth() + " x " + image.getHeight());
        long startTime = System.currentTimeMillis();
        sch.extract(image);
        System.out.println("Extraction time " + feature + " : " + (System.currentTimeMillis()-startTime));
        //System.out.println("sch = " + sch.getStringRepresentation());
    }


    public void testRetrieval(String feature) throws Exception {

        long averageExtractionTime = 0;

        LireFeature[] acc = new LireFeature[735];
        LinkedList<String> vds = new LinkedList<String>();
        ArrayList<String> names = new ArrayList<String>();

        int largeCount = 0;

        for (int i = 0; i < testFiles.length; i++) {


            int rows = 4; //You should decide the values for rows and cols variables
            int cols = 4;
            int chunks = rows * cols + 33;




            long startTime = System.currentTimeMillis();
            //LireFeature feat = null; //new FCTH();
            //feat = (LireFeature) Class.forName("net.semanticmetadata.lire.imageanalysis." + feature).newInstance();
            //  System.out.println("Extracting from number " + i);

            //acc[i].extract(ImageIO.read(new FileInputStream(testFilesPath + testFiles[i])));
            averageExtractionTime+=(System.currentTimeMillis()-startTime);
            // vds.add(acc[i].getStringRepresentation());

            File file = new File(testFilesPath + testFiles[i]); // I have bear.jpg in my working directory
            FileInputStream fis = new FileInputStream(file);
            BufferedImage image = ImageIO.read(fis); //reading the image file

            int chunkWidth = image.getWidth() / cols; // determines the chunk width and height
            int chunkHeight = image.getHeight() / rows;
            int count = 0;

            BufferedImage imgs[] = new BufferedImage[chunks]; //Image array to hold image chunks
            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols; y++) {
                    //Initialize the image array with image chunks
                    imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

                    // draws the image chunk
                    Graphics2D gr = imgs[count++].createGraphics();
                    gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x, chunkWidth * y + chunkWidth, chunkHeight * x + chunkHeight, null);
                    gr.dispose();
                }
            }

            //splitting 2

            for (int x = 0; x < rows; x++) {
                for (int y = 0; y < cols-1; y++) {
                    //Initialize the image array with image chunks
                    imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

                    // draws the image chunk
                    Graphics2D gr = imgs[count++].createGraphics();
                    gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, (chunkWidth * y)+(chunkWidth/2), chunkHeight * x, (chunkWidth * y)+(chunkWidth/2) + chunkWidth, chunkHeight * x + chunkHeight, null);
                    gr.dispose();
                }
            }

            //splitting 2

            for (int x = 0; x < rows-1; x++) {
                for (int y = 0; y < cols; y++) {
                    //Initialize the image array with image chunks
                    imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

                    // draws the image chunk
                    Graphics2D gr = imgs[count++].createGraphics();
                    gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y, chunkHeight * x+chunkHeight/2, chunkWidth * y + chunkWidth, (chunkHeight * x)+(chunkHeight/2) + chunkHeight, null);
                    gr.dispose();
                }
            }

            //splitting 2

            for (int x = 0; x < rows-1; x++) {
                for (int y = 0; y < cols-1; y++) {
                    //Initialize the image array with image chunks
                    imgs[count] = new BufferedImage(chunkWidth, chunkHeight, image.getType());

                    // draws the image chunk
                    Graphics2D gr = imgs[count++].createGraphics();
                    gr.drawImage(image, 0, 0, chunkWidth, chunkHeight, chunkWidth * y + chunkWidth/2, chunkHeight * x+chunkHeight/2, (chunkWidth * y + chunkWidth/2) + chunkWidth, (chunkHeight * x+chunkHeight/2) + chunkHeight, null);
                    gr.dispose();
                }
            }

            System.out.println("Splitting done");

            //writing mini images into image files
            for (int ii = 0; ii < imgs.length; ii++) {
                LireFeature feat = null; //new FCTH();
                feat = (LireFeature) Class.forName("net.semanticmetadata.lire.imageanalysis." + feature).newInstance();
                acc[largeCount] = feat;
                acc[largeCount].extract(imgs[ii]);
                vds.add(acc[largeCount].getStringRepresentation());
                ImageIO.write(imgs[ii], "jpg", new File("img" + ii + "_"+testFiles[i]));
                names.add(("img" + ii + "_"+testFiles[i]));
               // System.out.println("Subimage extracted");
                largeCount++;
            }



        }
        System.out.println("Average Extraction time " + feature + " : " + averageExtractionTime/10);

        // Not needed so far, maybe later
        System.out.println("Calculating distance for " + names.get(59));
        System.out.println(acc.length);




        //HMTLTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT
        String fileName = "classifieresults-" + System.currentTimeMillis() / 1000 + ".html";
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.write("<html>\n" +
                    "<head><title>Classification Results</title></head>\n" +
                    "<body bgcolor=\"#FFFFFF\">\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.write("<table>");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // int elems = Math.min(filesHTML.size(),50);
        int elems = acc.length;
        //HTMLTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT



         for (int i = 0; i < acc.length; i++) {
            float distance = acc[i].getDistance(acc[59]);
             System.out.println("distance = " + names.get(i) + " " + distance);

         }

        //HTML
        for (int i = 0; i < elems; i++) {
            if (i % 3 == 0) try {
                bw.write("<tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }

            String s = names.get(i); //"E:\\liretestfiles\\" +
            String colorF = "rgb(0, 255, 0)";

               if (acc[i].getDistance(acc[59])==0.0)
                   colorF = "rgb(255, 0, 0)";
            //  String s = reader.document(topDocs.scoreDocs[i].doc).get("descriptorImageIdentifier");
            //  System.out.println(reader.document(topDocs.scoreDocs[i].doc).get("featLumLay"));
            //  s = new File(s).getAbsolutePath();
            // System.out.println(s);
            try {
                bw.write("<td><a href=\"" + s + "\"><img title=\""+ "distance: "+ acc[i].getDistance(acc[59]) + "\" style=\"max-width:220px;border:medium solid " + colorF + ";\"src=\"" + s + "\" border=\"" + 5 + "\" style=\"border: 3px\n" +
                        "black solid;\"><p>"+names.get(i)+" distance: "+acc[i].getDistance(acc[59])+"</p></a></td>\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (i % 3 == 2) try {
                bw.write("</tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (elems % 3 != 0) {
            if (elems % 3 == 2) {
                try {
                    bw.write("<td>-</td with exit code 0\nd>\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bw.write("<td>-</td>\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (elems % 3 == 2) {
                try {
                    bw.write("<td>-</td>\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                bw.write("</tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            bw.write("</table></body>\n" +
                    "</html>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //HTML


    }

    //Create HTML
    public void  createHTML() {

        String fileName = "classifieresults-" + System.currentTimeMillis() / 1000 + ".html";
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.write("<html>\n" +
                    "<head><title>Classification Results</title></head>\n" +
                    "<body bgcolor=\"#FFFFFF\">\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.write("<table>");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // int elems = Math.min(filesHTML.size(),50);
        int elems = testFiles.length;

        for (int i = 0; i < elems; i++) {
            if (i % 3 == 0) try {
                bw.write("<tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }

            String s = "E:\\liretestfiles\\" + testFiles[i];
            String colorF = "rgb(0, 255, 0)";

         //   if (classesHTML.get(i).equals("no"))
         //       colorF = "rgb(255, 0, 0)";
            //  String s = reader.document(topDocs.scoreDocs[i].doc).get("descriptorImageIdentifier");
            //  System.out.println(reader.document(topDocs.scoreDocs[i].doc).get("featLumLay"));
            //  s = new File(s).getAbsolutePath();
            // System.out.println(s);
            try {
                bw.write("<td><a href=\"" + s + "\"><img title=\""+ "holder" + "\" style=\"max-width:220px;border:medium solid " + colorF + ";\"src=\"" + s + "\" border=\"" + 5 + "\" style=\"border: 3px\n" +
                        "black solid;\"></a></td>\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (i % 3 == 2) try {
                bw.write("</tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (elems % 3 != 0) {
            if (elems % 3 == 2) {
                try {
                    bw.write("<td>-</td with exit code 0\nd>\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bw.write("<td>-</td>\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (elems % 3 == 2) {
                try {
                    bw.write("<td>-</td>\n");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                bw.write("</tr>");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            bw.write("</table></body>\n" +
                    "</html>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
